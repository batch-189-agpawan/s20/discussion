// alert('hello')

/*
	While loop

		Syntax:
			While(expression/condition) {
					statement
			}
*/

let count = 5

while (count !== 0) {
	console.log('While: ' + count)
	count-- 
	// or count -=1
}

/*
	Mini activity
		the while loop should only display the numbers 1-5
		correct the following loop

		let x = 0
		while (x < 1) (
			console.log(x)
			x--
		)
*/

		let x = 1
		while (x <= 5) {
			console.log(x)
			x++
		}


/*
	DO WHILE LOOP
		A do-while loop works a lot like the while loop. But Unlike while loops, do-while loops guarantee that the code will be executed at least once.

		Syntax:
			do {
				statement

			} while (expression/condition)
*/


// let number = Number(prompt('Give me a number: '))

// do	{
// 	console.log('Do while: ' + number)
// 	number +=1
// }	while(number < 10)


/*
	FOR LOOP
		-more flexible than while loop and do-while loop

		SYNTAX:
			for (initialization, expression/condistion; finalexpression)
*/
for(let count = 0; count <=20; count++){
	console.log('for loop ' + count)
}

let myString = 'alex'
console.log(myString.length)

console.log(myString[0])

for(let x = 0 ; x<myString.length; x++){
	console.log(myString[x])
}

let myName = 'JEREMIAH'

for(let i = 0; i<myName.length; i++) {

	if(
		myName[i].toLowerCase()=='a' ||
		myName[i].toLowerCase()=='e' ||
		myName[i].toLowerCase()=='i' ||
		myName[i].toLowerCase()=='o' ||
		myName[i].toLowerCase()=='u' 
	)	{
		console.log('vowel')
	} else {
		console.log(myName[i])
	}
}


// CONTINUE AND BREAK STATEMENTS
/*
	"Continue" statement allows the code to go to the next iteration of the loop without finishing the execution of all statements in a code block

	"Break" statement is used to terminate the current loop once a match has been found.
*/


for (let count = 0; count <= 20; count++) {

	console.log('Hello World '+ count)

	if (count % 2 === 0) {
		console.log ('Even Number')
		continue;
	}

	console.log('Continue and Break: ' + count)

	if(count >10) {
		break;
	}
}



let name = 'Alexandro'

for (let i = 0; i < name.length; i++) {

	console.log(name[i]);

	if(name[i].toLowerCase() === "a") {
		console.log('Continue to the next iteration')
		continue;
	}

	if(name[i].toLowerCase() === "d") {
		break;
	}
}